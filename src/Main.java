import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by furan on 5/12/15.
 */
public class Main {
    public static void main(String[] args) {
        try {
            String str = "(1.0+1)*";

            // convert String into InputStream
            InputStream is = new ByteArrayInputStream(str.getBytes());

            // read it with BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(is));


            parser p = new parser(new Lexer(br));
            Node result = (Node)p.parse().value;

            System.out.println(result.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
